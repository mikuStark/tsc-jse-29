package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.IUserService;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {
    private final IUserRepository userRepository;
    private final IPropertyService propertyService;

    public UserService(IUserRepository userRepository, IPropertyService propertyService) {
        super((IRepository) userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public void addAll(List<User> users) {
        if (users == null) return;
        userRepository.addAll(users);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public boolean isLoginExists(@NonNull final String login) {
        if (login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    public User findByEmail(@NonNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(@NonNull final String email) {
        if (email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(@NonNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(@NonNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(@NonNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(@NonNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt((ISaltSettings) propertyService, password));
        return userRepository.add(user);
    }

    @Override
    public User create(@NonNull final String login, @NonNull final String password, @NonNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(@NonNull final String login, @NonNull final String password, @NonNull final Role role) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(@NonNull final String userId, @NonNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt((ISaltSettings) propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(
            @NonNull final String userId,
            @NonNull final String firstName,
            @NonNull final String lastName,
            @Nullable final String middleName) {
        if (userId.isEmpty()) throw new EmptyIdException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User lockUserByLogin(@NonNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(@NonNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }
}

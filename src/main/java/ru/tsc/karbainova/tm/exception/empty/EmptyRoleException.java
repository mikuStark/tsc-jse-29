package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {
    public EmptyRoleException() {
        super("Error Role.");
    }

    public EmptyRoleException(String value) {
        super("Error Role. " + value);
    }
}
